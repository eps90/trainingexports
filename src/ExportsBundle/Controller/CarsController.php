<?php

namespace ExportsBundle\Controller;

use ExportsBundle\Services\Cars;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\Response;

class CarsController
{
    private $templating;
    private $cars;

    public function __construct(TwigEngine $templating, Cars $cars)
    {
        $this->templating = $templating;
        $this->cars = $cars;
    }

    public function indexAction($viewName, $extension = 'xml')
    {
        return $this->render('ExportsBundle:Cars:' . $viewName . '.' . $extension . '.twig', ['cars' => $this->cars->getCars()], $extension);
    }

    private function render($view, $params, $extension)
    {
        $contentType = array(
            'xml' => 'application/xml',
            'html' => 'text/html',
        );

        return $this->templating->renderResponse($view, $params, new Response('', 200, ['Content-type' => $contentType[$extension]]));
    }
}
