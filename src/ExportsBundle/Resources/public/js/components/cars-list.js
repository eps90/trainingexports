angular.module('exports')
    .directive('cars', ['cars', '$filter', function (cars, $filter) {
        return {
            restrict: 'E',
            templateUrl: '/partials/cars-list.html',
            link: function (scope) {
                var orderBy = $filter('orderBy');

                scope.currentSortFieldname = 'price';
                scope.cars = cars;

                scope.order = function (fieldName) {
                    scope.currentSortFieldname = fieldName;
                    scope.reverse = (scope.currentSortFieldname === fieldName) ? !scope.reverse : false;
                    scope.cars = orderBy(scope.cars, fieldName, scope.reverse);
                }
                scope.order('price', false);
            }
        }
    }]);