<?php

use ExportsBundle\Controller\CarsController;
use ExportsBundle\Services\Cars;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\Response;

class CarsControllerTest extends WebTestCase
{
    private $templating;
    private $cars;

    public function setUp()
    {
        $this->templating = $this->getMockBuilder(TwigEngine::class)
            ->disableOriginalConstructor()
            ->getMock();
        
        $this->cars       = $this->getMockBuilder(Cars::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->cars->expects($this->once())
            ->method('getCars')
            ->willReturn([]);
    }

    /**
     * @test
     */
    public function itShouldRenderXmlTemplateWithCarsList()
    {
        $expectedResponse = new Response();
        $testInstance = $this;

        $this->templating->expects($this->once())
            ->method('renderResponse')
            ->with(
                $this->anything(),
                $this->equalTo(
                    [
                        'cars' => []
                    ]
                ),
                $this->callback(function (Response $Response) use ($testInstance) {
                    $testInstance->assertEquals('application/xml', $Response->headers->get('Content-Type'));
                    
                    return true;
                })
            )
            ->willReturn($expectedResponse);

        $cars = new CarsController($this->templating, $this->cars);
        $actualResponse = $cars->indexAction(null, 'xml');

        $this->assertSame($expectedResponse, $actualResponse);
    }

    /**
     * @test
     */
    public function itShouldRenderHtmlTemplateWithCarsList()
    {
        $expectedResponse = new Response();
        $testInstance = $this;

        $this->templating->expects($this->once())
            ->method('renderResponse')
            ->with(
                $this->anything(),
                $this->equalTo(
                    [
                        'cars' => []
                    ]
                ),
                $this->callback(function (Response $Response) use ($testInstance) {
                    $testInstance->assertEquals('text/html', $Response->headers->get('Content-Type'));
                    
                    return true;
                })
            )
            ->willReturn($expectedResponse);

        $cars           = new CarsController($this->templating, $this->cars);
        $actualResponse = $cars->indexAction(null, 'html');

        $this->assertSame($expectedResponse, $actualResponse);
    }
}
